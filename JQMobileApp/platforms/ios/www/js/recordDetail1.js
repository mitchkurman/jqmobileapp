$(document).ready(function(){
  // $('#show-detail-record').append("<b>"+getRecord()+"</b>");
  getRecord();
});

function getParameter(paramName){
  var searchString = window.location.search.substring(1), i, val, params = searchString.split("&");
  for (i=0;i<params.length;i++){
    val = params[i].split("=");
    if (val[0] == paramName){
      return val[1];
    }
  }
  return null;
};
function getRecord(){
  $.ajax({
      url: "https://oq88q01z9j.execute-api.us-east-1.amazonaws.com/prod/yu-anime-record/"+getParameter('uuId') ,
      dataType: "json",
      async: true,
      success: function (result) {
        // console.log(result);
        console.log(result.records[0]);
        var obj = result.records[0];
        $('#show-detail-record').append('<section id="about" class="text"><div class="container"><div class="row"><div class="col-md-6">');
        $('#show-detail-record').append('<h2 class="heading">'+obj.title+'</h2>');
        $('#show-detail-record').append('<h3 class="heading">'+obj.episode+'</h3>');
        $('#show-detail-record').append('<p>'+obj.description+'</p></div>');
        $('#show-detail-record').append('<div class="col-md-5 col-md-offset-1">');
        $('#show-detail-record').append('<p><img class="img-responsive img-rounded" src="http://yu-app.s3.amazonaws.com/test/'+obj.imageKey+'"></p>');
        $('#show-detail-record').append('</div></div></div></section>');
      },
      error: function (request,error) {
          alert('Network error has occurred please try again!');
      }
  });
}
